package android.yulius.com.crud_yulius.Utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.TextView;
import android.yulius.com.crud_yulius.DetailActivity;
import android.yulius.com.crud_yulius.R;
import android.yulius.com.crud_yulius.model.Contact;
import com.bumptech.glide.Glide;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@Layout(R.layout.load_more_item_view)
public class ItemView {

        @View(R.id.titleTxt)
        private TextView titleTxt;

        @View(R.id.cardView)
        private CardView card;

        @View(R.id.imageView)
        private ImageView imageView;

        private Contact mInfo;
        private Context mContext;
        private int pos;

        public ItemView(Context context, Contact info, int position) {
            mContext = context;
            mInfo = info;
            pos = position;
        }

        @Resolve
        private void onResolved() {
            Typeface font = Typeface.createFromAsset(mContext.getAssets(),
                    "Montserrat.ttf");

            titleTxt.setText(mInfo.getFirstname() + " " + mInfo.getLastname());
            titleTxt.setTypeface(font);


            if(!mInfo.getPhoto().isEmpty()) {
                Glide.with(mContext)
                        .load(mInfo.getPhoto())
                        .into(imageView);
            }

            card.setOnClickListener(new android.view.View.OnClickListener() {
                @Override
                public void onClick(android.view.View v) {
                    Intent intent = new Intent(mContext, DetailActivity.class);
                    intent.putExtra("position", pos);
                    mContext.startActivity(intent);
                }
            });
        }
    }

