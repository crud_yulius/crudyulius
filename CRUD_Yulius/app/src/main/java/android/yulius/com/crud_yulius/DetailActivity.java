package android.yulius.com.crud_yulius;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.yulius.com.crud_yulius.Utils.*;
import android.yulius.com.crud_yulius.model.Contact;
import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class DetailActivity extends AppCompatActivity {

    private Contact contact;
    private EditText photoURL;
    private EditText firstname;
    private EditText lastname;
    private EditText age;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setTitle("Detail Contact");

        Toolbar topToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);
        topToolBar.setTitleTextColor(Color.WHITE);
        topToolBar.setSubtitleTextColor(Color.WHITE);
        topToolBar.setLogoDescription("Detail Contact");

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Typeface font = Typeface.createFromAsset(this.getAssets(),
                "Montserrat.ttf");

        photoURL = (EditText) findViewById(R.id.photo_url);
        photoURL.setTypeface(font);
        firstname = (EditText) findViewById(R.id.firstname);
        firstname.setTypeface(font);
        lastname = (EditText) findViewById(R.id.lastname);
        lastname.setTypeface(font);
        age = (EditText) findViewById(R.id.age);
        age.setTypeface(font);
        imageView = (ImageView) findViewById(R.id.imageView);

        TextView phototxt = (TextView) findViewById(R.id.phototext);
        phototxt.setTypeface(font);
        TextView firsttxt = (TextView) findViewById(R.id.firstnametxt);
        firsttxt.setTypeface(font);
        TextView lasttxt = (TextView) findViewById(R.id.lastnametxt);
        lasttxt.setTypeface(font);
        TextView agetxt = (TextView) findViewById(R.id.agetxt);
        agetxt.setTypeface(font);

        if (getIntent().getExtras() != null) {
            if (getIntent().hasExtra("position")) {
                int position = getIntent().getExtras().getInt("position");
                contact = Utils.contacts.get(position);

                photoURL.setText( "" + contact.getPhoto());
                firstname.setText(contact.getFirstname());
                lastname.setText(contact.getLastname());
                age.setText("" + contact.getAge());

                if (!contact.getPhoto().isEmpty()) {
                    Glide.with(this)
                            .load(contact.getPhoto())
                            .into(imageView);
                }
            }
        }

        Button save = (Button) findViewById(R.id.btn_save);
        save.setTypeface(font);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstname.getText().toString().isEmpty() || lastname.getText().toString().isEmpty() ||
                        age.getText().toString().isEmpty()){
                    MyToast.showToast(DetailActivity.this, "Field cannot be empty");
                    return;
                }
                save();
            }
        });

        final Button delete = (Button) findViewById(R.id.btn_delete);
        delete.setTypeface(font);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });

    }

    private void save() {
        try {
            JSONObject obj = new JSONObject();
            obj.put("photo", photoURL.getText().toString());
            obj.put("firstName", firstname.getText().toString());
            obj.put("lastName", lastname.getText().toString());
            obj.put("age", age.getText().toString());

            MyLoadingDialog.MyDialog.showDialogProgress(DetailActivity.this, "");

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.PUT, URL.CONTACT + "/" + contact.getId(), obj,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.e("UPDATE", "" + response);
                                    MyLoadingDialog.MyDialog.hideDialogProgress();

                                    try {
                                        JSONObject objparent = new JSONObject(String.valueOf(response));

                                        if (response == null) {
                                            MyToast.showToast(DetailActivity.this, "Could not update contact");
                                            return;
                                        }

                                        String message = objparent.getString("message");

                                        if (message.equals("contact edited")) {
                                            MyToast.showToast(DetailActivity.this, message);
                                        } else {
                                            MyToast.showToast(DetailActivity.this, message);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            MyLoadingDialog.MyDialog.hideDialogProgress();
                            VolleyHandler.handleVolleyError(DetailActivity.this, error);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    return headers;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }
            };

            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppConstant.TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            VolleyRequest.getInstance().addToRequestQueue(jsObjRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void delete() {
        try {
            MyLoadingDialog.MyDialog.showDialogProgress(DetailActivity.this, "");

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.DELETE, URL.CONTACT +"/"+ contact.getId(), null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.e("DELETE", "" + response);
                                    MyLoadingDialog.MyDialog.hideDialogProgress();

                                    try {
                                        JSONObject objparent = new JSONObject(String.valueOf(response));

                                        if (response == null) {
                                            MyToast.showToast(DetailActivity.this, "Could not delete contact");
                                            return;
                                        }

                                        String message = objparent.getString("message");

                                        if (message.equals("contact deleted")) {
                                            MyToast.showToast(DetailActivity.this, message);
                                            Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            MyToast.showToast(DetailActivity.this, message);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            MyLoadingDialog.MyDialog.hideDialogProgress();
                            NetworkResponse response = error.networkResponse;
                            if (error instanceof ServerError && response != null) {
                                try {
                                    String res = new String(response.data,
                                            HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                    JSONObject obj = new JSONObject(res);

                                    String message = obj.getString("message");
                                    MyToast.showToast(DetailActivity.this, message);
                                } catch (UnsupportedEncodingException e1) {
                                    e1.printStackTrace();
                                } catch (JSONException e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };

            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppConstant.TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            VolleyRequest.getInstance().addToRequestQueue(jsObjRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // goto back activity from here
                Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
