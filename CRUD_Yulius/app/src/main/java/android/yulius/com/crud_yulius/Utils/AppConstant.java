package android.yulius.com.crud_yulius.Utils;

public class AppConstant {

    //Error message volley
    public static final String messageNetworkError      = "Tidak dapat terhubung ke Internet .. Silakan periksa koneksi anda!";
    public static final String messageServerError       = "Server tidak dapat ditemukan. Silakan coba lagi setelah beberapa waktu";
    public static final String messageAuthFailureError  = "Tidak dapat terhubung ke Internet .. Silakan periksa koneksi anda!";
    public static final String messageParseError        = "Gagal menerjemahkan pesan dari server! Silakan coba lagi setelah beberapa waktu";
    public static final String messageTimeoutError      = "Waktu koneksi habis! Silakan periksa koneksi internet anda";


    public static final int TIMEOUT=5000;
}
