package android.yulius.com.crud_yulius.Utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDialog;
import android.widget.TextView;
import android.yulius.com.crud_yulius.R;
import com.wang.avi.AVLoadingIndicatorView;

public class MyLoadingDialog extends AppCompatDialog {

    TextView tv_title_mydialog;

    AVLoadingIndicatorView aviLoading;

    public MyLoadingDialog(Context context) {
        super(context, R.style.TransparentDialogTheme);
        setContentView(R.layout.my_loading_dialog);

        TextView tv_title_mydialog = (TextView) findViewById(R.id.tv_title_mydialog);

        AVLoadingIndicatorView aviLoading = (AVLoadingIndicatorView) findViewById(R.id.aviLoading);

        aviLoading.setIndicator("BallScaleMultipleIndicator");
        aviLoading.setIndicatorColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));

        setCanceledOnTouchOutside(true);
    }

    public void updateMessage(String val) {
        tv_title_mydialog.setText(val);
    }

    public static class MyDialog {

        public static MyLoadingDialog progressDialog;

        public static void showDialogProgress(Context context, String message) {
            progressDialog = new MyLoadingDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        public static void hideDialogProgress(){
            if(progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }

}

