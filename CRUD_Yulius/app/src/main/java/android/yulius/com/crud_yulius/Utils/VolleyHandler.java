package android.yulius.com.crud_yulius.Utils;

import android.content.Context;
import com.android.volley.*;

import static android.yulius.com.crud_yulius.Utils.AppConstant.*;
import static android.yulius.com.crud_yulius.Utils.MyToast.showToast;

public class VolleyHandler {

    public static void handleVolleyError(Context context, VolleyError error) {

        String message = "";

        if (error instanceof NetworkError) {
            message = messageNetworkError;
            MyLoadingDialog.MyDialog.hideDialogProgress();
        } else if (error instanceof ServerError) {
            message = messageServerError;
            MyLoadingDialog.MyDialog.hideDialogProgress();
        } else if (error instanceof AuthFailureError) {
            message = messageAuthFailureError;
            MyLoadingDialog.MyDialog.hideDialogProgress();
        } else if (error instanceof ParseError) {
            message = messageParseError;
            MyLoadingDialog.MyDialog.hideDialogProgress();
        } else if (error instanceof TimeoutError) {
            message = messageTimeoutError;
            MyLoadingDialog.MyDialog.hideDialogProgress();
        }

        showToast(context,message);
    }

}