package android.yulius.com.crud_yulius.Utils;

import android.content.Context;
import android.widget.Toast;

public class MyToast {

    private static Toast mToast;

    public static void showToast(Context context, String message){

        if(mToast != null) {
            mToast.cancel();
        }

        mToast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        mToast.show();
    }


}
