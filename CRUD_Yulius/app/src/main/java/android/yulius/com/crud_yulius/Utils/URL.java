package android.yulius.com.crud_yulius.Utils;

public class URL {

    public static final String ROOT_API = "https://simple-contact-crud.herokuapp.com";

    public static final String CONTACT = ROOT_API + "/contact";
}
