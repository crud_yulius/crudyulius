package android.yulius.com.crud_yulius;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.yulius.com.crud_yulius.Utils.*;
import android.yulius.com.crud_yulius.model.Contact;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mindorks.placeholderview.InfinitePlaceHolderView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipeLayout;
    private InfinitePlaceHolderView mLoadMoreView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Contact");

        Toolbar topToolBar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);
        topToolBar.setTitleTextColor(Color.WHITE);
        topToolBar.setSubtitleTextColor(Color.WHITE);

        mLoadMoreView = (InfinitePlaceHolderView)findViewById(R.id.loadMoreView);
        topToolBar.setLogoDescription("Contact");

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mLoadMoreView.removeAllViews();
                getContact();

                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        swipeLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        // Scheme colors for animation
        swipeLayout.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_blue_bright),
                getResources().getColor(android.R.color.holo_green_light),
                getResources().getColor(android.R.color.holo_orange_light),
                getResources().getColor(android.R.color.holo_red_light)
        );

        getContact();

    }


    //Get Promo List
    private void getContact() {
        Utils.contacts.clear();
        MyLoadingDialog.MyDialog.showDialogProgress(this, "");
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (URL.CONTACT, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("GET Contact", "" + response);
                        MyLoadingDialog.MyDialog.hideDialogProgress();

                        try {
                            JSONObject objparent = new JSONObject(String.valueOf(response));

                            if (response == null) {
                                MyToast.showToast(MainActivity.this, "Could not refresh data");
                                return;
                            }

                            String message = objparent.getString("message");
                            if (message.equals("Get contacts")) {
                                JSONArray listvalue = objparent.getJSONArray("data");

                                for (int i = 0; i < listvalue.length(); i++) {
                                    JSONObject data = listvalue.getJSONObject(i);
                                    Contact contact = new Contact();
                                    contact.setId(data.getString("id"));
                                    contact.setFirstname(data.getString("firstName"));
                                    contact.setLastname(data.getString("lastName"));
                                    contact.setAge(data.getInt("age"));
                                    contact.setPhoto(data.getString("photo"));
                                    Utils.contacts.add(contact);

                                    mLoadMoreView.addView(new ItemView(MainActivity.this, contact, i));
                                }

                            } else {
                                MyToast.showToast(MainActivity.this, message);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        MyLoadingDialog.MyDialog.hideDialogProgress();
                        VolleyHandler.handleVolleyError(MainActivity.this, error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                AppConstant.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        VolleyRequest.getInstance().addToRequestQueue(jsObjRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            startActivity(new Intent(MainActivity.this, AddContactActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
