package android.yulius.com.crud_yulius;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.yulius.com.crud_yulius.Utils.*;
import android.yulius.com.crud_yulius.model.Contact;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddContactActivity extends AppCompatActivity {

    private Contact contact;
    private EditText photoURL;
    private EditText firstname;
    private EditText lastname;
    private EditText age;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        setTitle("Add Contact");

        Toolbar topToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);
        topToolBar.setTitleTextColor(Color.WHITE);
        topToolBar.setSubtitleTextColor(Color.WHITE);
        topToolBar.setLogoDescription("Add Contact");

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Typeface font = Typeface.createFromAsset(this.getAssets(),
                "Montserrat.ttf");

        photoURL = (EditText) findViewById(R.id.photo_url);
        photoURL.setTypeface(font);
        firstname = (EditText) findViewById(R.id.firstname);
        firstname.setTypeface(font);
        lastname = (EditText) findViewById(R.id.lastname);
        lastname.setTypeface(font);
        age = (EditText) findViewById(R.id.age);
        age.setTypeface(font);
        imageView = (ImageView) findViewById(R.id.imageView);

        TextView phototxt = (TextView) findViewById(R.id.phototext);
        phototxt.setTypeface(font);
        TextView firsttxt = (TextView) findViewById(R.id.firstnametxt);
        firsttxt.setTypeface(font);
        TextView lasttxt = (TextView) findViewById(R.id.lastnametxt);
        lasttxt.setTypeface(font);
        final TextView agetxt = (TextView) findViewById(R.id.agetxt);
        agetxt.setTypeface(font);

        Button save = (Button) findViewById(R.id.btn_save);
        save.setTypeface(font);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstname.getText().toString().isEmpty() || lastname.getText().toString().isEmpty() ||
                        age.getText().toString().isEmpty()){
                    MyToast.showToast(AddContactActivity.this, "Field cannot be empty");
                    return;
                }
                save();
            }
        });
    }

    private void save() {
        try {
            JSONObject obj = new JSONObject();
            String url = "";
            if (photoURL.getText().toString().isEmpty()){
                url = "N/A";
            } else {
                photoURL.getText().toString();
            }
            obj.put("photo", url);
            obj.put("firstName", firstname.getText().toString());
            obj.put("lastName", lastname.getText().toString());
            obj.put("age", age.getText().toString());

            MyLoadingDialog.MyDialog.showDialogProgress(AddContactActivity.this, "");

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (URL.CONTACT, obj,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.e("ADD", "" + response);
                                    MyLoadingDialog.MyDialog.hideDialogProgress();

                                    try {
                                        JSONObject objparent = new JSONObject(String.valueOf(response));

                                        if (response == null) {
                                            MyToast.showToast(AddContactActivity.this, "Could not a coddntact");
                                            return;
                                        }

                                        String message = objparent.getString("message");

                                        if (message.equals("contact saved")) {
                                            MyToast.showToast(AddContactActivity.this, message);
                                            Intent intent = new Intent(AddContactActivity.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            MyToast.showToast(AddContactActivity.this, message);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            MyLoadingDialog.MyDialog.hideDialogProgress();
                            VolleyHandler.handleVolleyError(AddContactActivity.this, error);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    return headers;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }
            };

            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                    AppConstant.TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            VolleyRequest.getInstance().addToRequestQueue(jsObjRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // goto back activity from here
                Intent intent = new Intent(AddContactActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}